import React, { Component } from 'react';
import './App.css';
import { Switch, Route ,Link} from 'react-router-dom'
import { elastic as Menu } from 'react-burger-menu';

import Home from './routes/Home';
import Experiment from './routes/Experiment';



class App extends Component {
  render() {
    return (
      <div id="outer-container">

        <Menu  pageWrapId={"page-wrap"} outerContainerId={"outer-container"}>
          {/* <a id="home" className="menu-item" href="/"><span>Home</span></a>
          <a id="about" className="menu-item" href="/experiment">Experiment</a> */}

          <Link to = "/">Home</Link>
          <Link to = "/experiment">Experiment</Link>
         
        </Menu>
        <main id="page-wrap">
        <div>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/experiment' component={Experiment} />
          </Switch>
          </div>
        </main>
      </div>
    )
  }
}

export default App;