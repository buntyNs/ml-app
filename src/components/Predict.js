import React from 'react';
import './form.css'
import Loader from 'react-loader-spinner'
import { Col, Row, Button, Form, FormGroup, Label, Input,Container, Card,CardBody } from 'reactstrap';


const Predict = (props) => {
  return (
    <Container>
        <Card className="card-style">
            <CardBody>
                {
                    props.loading ? <Loader 
                    type="Oval"
                    color="#607D8B"
                    height="80"	
                    width="80"
                 />   : <h1>{props.predict}</h1>
                }
                
            </CardBody>
        </Card>
    </Container>


   
  )
}
export default Predict;
