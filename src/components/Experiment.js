import React from 'react';
import './form.css'
import { Col, Row, Button, Form, FormGroup, Label, Input, Container, Card, CardBody } from 'reactstrap';


const Experiment = (props) => {

    return (
        <Container>
            <h3 style={{ color: 'black' }}>Put features and see YOUR CAR price!!!</h3>
            <Card className="card-style">
                <CardBody>
                    <Form >
                        <Row form>

                            <Col md={3}>
                                <FormGroup>
                                    <Label for="exampleSelect">Brand</Label>
                                    <Input className="uppercase" onChange={(event) => { props.change(event) }} type="select" name="brand" id="exampleSelect">
                                        {props.selectBrand.map((Brand, index) => {
                                            return (
                                                <option key={index}>{Brand}</option>
                                            );
                                        })}

                                    </Input>
                                </FormGroup>
                            </Col>

                            <Col md={3}>
                                <FormGroup>
                                    <Label for="exampleAddress">Model</Label>
                                    <Input className="lowwercase" onChange={(event) => { props.change(event) }} type="select" name="model" id="exampleAddress" placeholder="Model">
                                        {props.selectModel.map((model, index) => {
                                            return (
                                                <option key={index}>{model}</option>
                                            );
                                        })}
                                    </Input>
                                </FormGroup>
                            </Col>
                            <Col md={3}>
                                <FormGroup>
                                    <Label for="exampleAddress">Transmission</Label>
                                    <Input onChange={(event) => { props.change(event) }} type="select" name="transmission" id="exampleAddress" placeholder="Transmission">
                                        {props.selectTrans.map((model, index) => {
                                            return (
                                                <option key={index}>{model}</option>
                                            );
                                        })}

                                    </Input>
                                </FormGroup>
                            </Col>
                            <Col md={3}>
                                <FormGroup>
                                    <Label for="exampleEmail">Model Year</Label>
                                    <Input value={props.initYear} onChange={(event) => { props.change(event) }} type="number" name="year" id="exampleEmail" placeholder="Model Year" />
                                </FormGroup>
                            </Col>

                        </Row>

                        <Row form>
                            <Col md={4}>
                                <FormGroup>
                                    <Label for="exampleEmail">Derivetrain</Label>
                                    <Input  onChange={(event) => { props.change(event) }} type="select" name="drivetrain" id="exampleEmail" placeholder="Derivetrain">
                                    {props.selectDrives.map((model, index) => {
                                            return (
                                                <option  key={index}>{model}</option>
                                            );
                                        })}
                                    </Input>
                                </FormGroup>
                            </Col>
                            <Col md={4}>
                                <FormGroup>
                                    <Label for="examplePassword">Fuel Type</Label>
                                    <Input onChange={(event) => { props.change(event) }} type="select" name="fuelType" id="examplePassword" placeholder="Fuel Type">
                                    {props.selectFuels.map((model, index) => {
                                            return (
                                                <option  key={index}>{model}</option>
                                            );
                                        })}
                                    </Input>
                                </FormGroup>
                            </Col>

                            <Col md={4}>
                                <FormGroup>
                                    <Label for="exampleAddress">Milege</Label>
                                    <Input value={props.initMileage} onChange={(event) => { props.change(event) }} type="number" name="mileage" id="exampleAddress" placeholder="Milege" />
                                </FormGroup>
                            </Col>
                        </Row>

                        <Row>
                            <Col md={6}>

                            </Col>
                            <Col md={6}>
                                <Button type="button" style={{ float: 'right' }} onClick={() => { props.send() }} >Predict</Button>
                                <Button type="button" style={{ float: 'right', marginRight: '10px' }} onClick={() => { props.reset() }}>Reset</Button>
                            </Col>

                        </Row>


                    </Form>
                </CardBody>
            </Card>
        </Container>
    );
}
export default Experiment;
