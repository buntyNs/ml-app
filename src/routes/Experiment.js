import React, { Component } from 'react';
import ExperimentView from '../components/Experiment';
import Predict from '../components/Predict';
import axios from 'axios';

class Experiment extends Component {
  constructor(props) {
    super(props);

    this.state = {
      drivetrain: "",
      fuelType: "",
      mileage: "",
      transmission: "",
      year: "",
      brand: "",
      model: "",
      selectBrand: [],
      selectModel: [],
      selectTrans: [],
      selectDrives: [],
      selectFuels: [],
      modelAvailale: false,
      predict: "Get your car price !",
      loading: false
    }
  }

  // data = [
  //   ["Nissan", ["Murano S", "Rogue SV", "Sentra", "Pathfinder SL"]],
  //   ["Toyota", ["corolla", "Corolla L", "Seinna XLE", "camry", "Tacoma SR", "supra"]],
  //   ["Ford", ["F-150 XLT", "Escape SE", "Flex Limited", "Edge Limited", "F-150 Platinum"]],
  //   ["Lexus", ["RC 350 Base", "RX 350 Base", "GS 350 Base", "LS 460 Base"]],
  //   ["Honda", ["Civic LX", "Odyssey EX-L", "Accord EX", "Accord Sport"]]
  // ];

  handleOnChange = (event) => {
    console.log(event.target.value);


    console.log(event.target.value);
    if (event.target.name === "brand") {

      axios.post(`http://enkhbat.pythonanywhere.com/cars/api/v1.0/models`, {
        "Make": event.target.value
      })
        .then(res => {
          console.log(res);
          // console.log(res.data.Makes);
          this.setState({

            selectModel: res.data.Models
          });

          axios.post(`http://enkhbat.pythonanywhere.com/cars/api/v1.0/fuels`, {
            "Model": res.data.Models[0]
          })
            .then(res => {
              console.log(res);
              // console.log(res.data.Makes);
              this.setState({

                selectFuels: res.data.Fuels,
                fuelType: res.data.Fuels[0]
              });

            })

            axios.post(`http://enkhbat.pythonanywhere.com/cars/api/v1.0/drives`, {
              "Model": res.data.Models[0]
            })
              .then(res => {
                console.log(res);
                // console.log(res.data.Makes);
                this.setState({

                  selectDrives: res.data.Trans,
                  drivetrain: res.data.Trans[0]
                });

              })

              axios.post(`http://enkhbat.pythonanywhere.com/cars/api/v1.0/trans`, {
                "Model": res.data.Models[0]
              })
                .then(res => {
                  console.log(res);
                  // console.log(res.data.Makes);
                  this.setState({
  
                    selectTrans: res.data.Trans,
                    transmission: res.data.Trans[0]
                  });
  
                })





        })
    }

    if (event.target.name === "model") {

      axios.post(`http://enkhbat.pythonanywhere.com/cars/api/v1.0/drives`, {
        "Model": event.target.value
      })
        .then(res => {
          console.log(res);
          // console.log(res.data.Makes);
          this.setState({

            selectDrives: res.data.Trans
          });

        })

      axios.post(`http://enkhbat.pythonanywhere.com/cars/api/v1.0/fuels`, {
        "Model": event.target.value
      })
        .then(res => {
          console.log(res);
          // console.log(res.data.Makes);
          this.setState({

            selectFuels: res.data.Fuels
          });

        })

      axios.post(`http://enkhbat.pythonanywhere.com/cars/api/v1.0/trans`, {
        "Model": event.target.value
      })
        .then(res => {
          console.log(res);
          // console.log(res.data.Makes);
          this.setState({

            selectTrans: res.data.Trans
          });

        })

    }
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  reset = () => {
    console.log("reste");
    this.setState({
      mileage: "",
      year: "",
      predict: "Get your car price !",
      loading: false

    });
  }

  send = () => {
    this.setState({
      loading: true

    });
    console.log(
      "Drivetrain", this.state.drivetrain,
      "Fuel Type", " " + this.state.fuelType,
      "Mileage", this.state.mileage,
      "Transmission", this.state.transmission,
      "Make", this.state.brand,
      "Model", this.state.model,
      "ModelYear", this.state.year
    );
    axios.post(`http://enkhbat.pythonanywhere.com/cars/api/v1.0/predic`, {

      "Drivetrain": this.state.drivetrain,
      "Fuel Type": this.state.fuelType,
      "Mileage": this.state.mileage,
      "Transmission": this.state.transmission,
      "Make": this.state.brand,
      "Model": this.state.model,
      "ModelYear": this.state.year

    })
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.setState({
          loading: false,
          predict: res.data.predicted_price
        });

      })
  }

  componentWillMount() {

    axios.get(`http://enkhbat.pythonanywhere.com/cars/api/v1.0/makes`, {})
      .then(res => {
        console.log(res);
        console.log(res.data.Makes);
        this.setState({

          selectBrand: res.data.Makes,
          brand: res.data.Makes[0]

        });

        axios.post(`http://enkhbat.pythonanywhere.com/cars/api/v1.0/models`, {
          "Make": res.data.Makes[0]
        })
          .then(res => {
            console.log(res);
            // console.log(res.data.Makes);
            this.setState({

              selectModel: res.data.Models,
              model: res.data.Models[0]
            });

            axios.post(`http://enkhbat.pythonanywhere.com/cars/api/v1.0/fuels`, {
              "Model": res.data.Models[0]
            })
              .then(res => {
                console.log(res);
                // console.log(res.data.Makes);
                this.setState({

                  selectFuels: res.data.Fuels,
                  fuelType: res.data.Fuels[0]
                });

              })


            axios.post(`http://enkhbat.pythonanywhere.com/cars/api/v1.0/drives`, {
              "Model": res.data.Models[0]
            })
              .then(res => {
                console.log(res);
                // console.log(res.data.Makes);
                this.setState({

                  selectDrives: res.data.Trans,
                  drivetrain: res.data.Trans[0]
                });

              })
            axios.post(`http://enkhbat.pythonanywhere.com/cars/api/v1.0/trans`, {
              "Model": res.data.Models[0]
            })
              .then(res => {
                console.log(res);
                // console.log(res.data.Makes);
                this.setState({

                  selectTrans: res.data.Trans,
                  transmission: res.data.Trans[0]
                });

              })

          })

      })




    // this.setState({
    //   selectModel: this.data[0][1]
    // });

  }
  render() {
    console.log(this.state.brand);
    return (
      <div>
        <ExperimentView
          change={this.handleOnChange}
          modelAvailale={this.state.modelAvailale}
          selectModel={this.state.selectModel}
          selectBrand={this.state.selectBrand}
          selectDrives={this.state.selectDrives}
          selectFuels={this.state.selectFuels}
          selectTrans={this.state.selectTrans}
          send={this.send}
          reset={this.reset}
          initDrive={this.state.drivetrain}
          initTrans={this.state.transmission}
          initYear={this.state.year}
          initFuel={this.state.fuelType}
          initMileage={this.state.mileage}
        />

        {
          <Predict loading={this.state.loading} predict={this.state.predict} />
        }
      </div>


    )
  }
}

export default Experiment;